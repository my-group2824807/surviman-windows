using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FatManMagn : MonoBehaviour
{
    public Transform Fatman;
  //  [SerializeField] private Animator _animatorFatMan;
    public float forceFatman;
    [SerializeField] private Animator _animatorFatMan;
    
    void Start()
    {
      
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<AudioSource>().Play();
            Dance();
            FindObjectOfType<PlayerMove>().Down();
            FindObjectOfType<GameManager>().ShowLoseWindow();
          
        }
    }

   
    // Update is called once per frame
    void Update()
    {
        //  transform.rotation = Quaternion.Euler(0f, 10f, 0f);
        Vector3 direction = Fatman.position - transform.position;
        GetComponent<Rigidbody>().AddForce(direction.normalized * Time.deltaTime * forceFatman); //, ForceMode.Impulse);
    }

    public void Dance()
    {
        _animatorFatMan.SetTrigger("Dance");
        transform.Rotate(0f, 180f, 0f);       
    }

    public void Run()
    {
        _animatorFatMan.SetBool("Run", true);
    }

   

}



