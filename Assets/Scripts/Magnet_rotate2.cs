using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet_rotate2 : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed;
   


    void Update()
    {
        transform.Rotate(0, _rotationSpeed * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
       //     GetComponent<AudioSource>().Play();
        //    Debug.Log(GetComponent<Collider>().bounds.size);
            Destroy(gameObject);
            GameObject obj = GameObject.Find("Player");
            obj.GetComponent<PlayerBehaviour>().force2 = 0;

            GameObject particleEffect = GameObject.Find("ElectricsWave2").gameObject;
            particleEffect.SetActive(false);
        //    GetComponent<AudioSource>().Play();
        }
    }
}
