using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    [SerializeField] PlayerMove _playerMove;
    [SerializeField] PreFinishBehaviour _preFinishBehaviour;
    [SerializeField] Animator _animatorTimmy;

    public Transform magnet1;  // ����������� �  1-�� �������
    public Transform magnet2;  // ����������� �� 2-�� �������
    public Transform magnet3;  // ����������� �  3-�� �������
    public Transform magnet4;  // ����������� �� 4-�� �������

    public float force = 5f;   // ������������ Player � ������� 1
    public float force2 = 5f;  // ������������ Player � ������� 2
    public float force3 = 5f;   // ������������ Player � ������� 3
    public float force4 = 5f;  // ������������ Player � ������� 4

    public float forceFatman = 1f;

    void Start()
    {
        _playerMove.enabled = false;
        _preFinishBehaviour.enabled = false;
    }



    void FixedUpdate()
    {
        Vector3 direction1 = magnet1.position - transform.position; // ����������� � 1-�� �������
        direction1.x = Mathf.Clamp(direction1.x, -2.6f, 2.6f);
        Vector3 direction2 = magnet2.position - transform.position; // ����������� � 2-�� �������
        direction2.x = Mathf.Clamp(direction2.x, -2.6f, 2.6f);
        Vector3 direction3 = magnet3.position - transform.position; // ����������� � 3-�� �������
        direction3.x = Mathf.Clamp(direction3.x, -2.6f, 2.6f);
        Vector3 direction4 = magnet4.position - transform.position; // ����������� � 4-�� �������
        direction4.x = Mathf.Clamp(direction4.x, -2.6f, 2.6f);

        GetComponent<Rigidbody>().AddForce(direction1.normalized * force + direction2.normalized * force2 + direction3.normalized * force3 + direction4.normalized * force4); 

        //  Vector3 direction2 = player.position - transform.position; // ����������� �� 2-�� �������
        //  GetComponent<Rigidbody>().AddForce(direction2.normalized * force2);
    }




    public void Play()
    {
        _playerMove.enabled = true;       
        GameObject obj = GameObject.Find("Fatman");       
        obj.GetComponent<FatManMagn>().transform.Rotate(0f, 180f, 0f);
        _playerMove.transform.Rotate(0f, 180f, 0f);
        obj.GetComponent<FatManMagn>().forceFatman = 900f; //700f;
        force = 5f;
        force2 = 5f;
        force3 = 5f;
        force4 = 5f;

    }

    public void StartPreFinishBehaviour()
    {
        _playerMove.enabled = false;
        _preFinishBehaviour.enabled = true;
    }

    public void StartFinishBehaviour()
    {
        _preFinishBehaviour.enabled = false;
        _animatorTimmy.SetTrigger("Dance");
        _animatorTimmy.transform.Rotate(0f, 180f, 0f);

    }

}
